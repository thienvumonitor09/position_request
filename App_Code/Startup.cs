﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PR2.Startup))]
namespace PR2
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
