﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Position
/// </summary>
public class Position
{

    private string positionID;
    private string officialTitle;
    private string workingTitle;
    private string college;
    private string classNo;
    private string budgetNo;
    private string positionControlNo;
    private string department;
    private string departmentNo;
    private string phoneNo;
    private string mailStop;
    private string supervisor;
    private string supervisorID;
    private string recruitmentType;
    private string positionType;
    private string newReplace;
    private string nameReplace;
    private string orginatorID;



    public string PositionID { get => positionID; set => positionID = value; }
    public string OfficialTitle { get => officialTitle; set => officialTitle = value; }
    public string WorkingTitle { get => workingTitle; set => workingTitle = value; }
    public string College { get => college; set => college = value; }
    public string ClassNo { get => classNo; set => classNo = value; }
    public string BudgetNo { get => budgetNo; set => budgetNo = value; }
    public string PositionControlNo { get => positionControlNo; set => positionControlNo = value; }
    public string Department { get => department; set => department = value; }
    public string DepartmentNo { get => departmentNo; set => departmentNo = value; }
    public string PhoneNo { get => phoneNo; set => phoneNo = value; }
    public string MailStop { get => mailStop; set => mailStop = value; }
    public string Supervisor { get => supervisor; set => supervisor = value; }
    public string SupervisorID { get => supervisorID; set => supervisorID = value; }
    public string RecruitmentType { get => recruitmentType; set => recruitmentType = value; }
    public string PositionType { get => positionType; set => positionType = value; }
    public string OrginatorID { get => orginatorID; set => orginatorID = value; }
    public string NameReplace { get => nameReplace; set => nameReplace = value; }
    public string NewReplace { get => newReplace; set => newReplace = value; }
}