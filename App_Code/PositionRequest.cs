﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for PositionRequest
/// </summary>
public class PositionRequest {

    private SqlConnection objCon;
    private String strSQL, strConnection;
    private SqlCommand objCmd;
    private SqlDataAdapter objDA;
    private DataSet objDS;

    public PositionRequest() {
        //Production connection string
        //strConnection = "Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=internalPositionRequest;Initial Catalog=CCSPositionRequest;Integrated Security=true;";

        //Development connection string
        strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalPositionRequest;Initial Catalog=CCSPositionRequest;Integrated Security=true;";
    }

    public String GetWelcomeText() {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetWelcomeText", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch (Exception) {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditWelcomeText(String welcomeText) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_UpdateWelcomeText", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@WelcomeText", SqlDbType.VarChar);
            objCmd.Parameters["@WelcomeText"].Value = welcomeText;

            Int32 intCtr = objCmd.ExecuteNonQuery();
            if (intCtr == 0) {
                return false;
            } else {
                return true;
            }
        } finally {
            objCon.Close();
        }
    }

    public String GetEmployeeName(String employeeID) {
        try {
            objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeeNameByID", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar);
            objCmd.Parameters["@EMPLID"].Value = employeeID;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public String GetUserPermission(String employeeID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetUserPermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar);
            objCmd.Parameters["@EMPLID"].Value = employeeID;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPermissions() {
        try {
            objCon = new SqlConnection(strConnection);
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPermissions", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPermission(Int32 permissionID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PermissionID", SqlDbType.Int);
            objCmd.Parameters["@Permission"].Value = permissionID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public bool AddPermission(String employeeID, String name, String permission) {
        objCon = new SqlConnection(strConnection);
        try { 
            objCmd = new SqlCommand("usp_AddPermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar);
            objCmd.Parameters.Add("@Name", SqlDbType.VarChar);
            objCmd.Parameters.Add("@Permission", SqlDbType.VarChar);
            objCmd.Parameters["@EMPLID"].Value = employeeID;
            objCmd.Parameters["@Name"].Value = name;
            objCmd.Parameters["@Permission"].Value = permission;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditPermission(Int32 permissionID, String permission) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_UpdatePermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PermissionID", SqlDbType.Int);
            objCmd.Parameters.Add("@Permission", SqlDbType.VarChar);
            objCmd.Parameters["@PermissionID"].Value = permissionID;
            objCmd.Parameters["@Permission"].Value = permission;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }
}