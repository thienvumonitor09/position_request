﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Employee
/// </summary>
public class Employee
{
    
    public string empId;
    public string empName;
    public string deptName;

    public Employee(string empId, string empName, string deptName)
    {
        this.empId = empId;
        this.empName = empName;
        this.deptName = deptName;
    }

}