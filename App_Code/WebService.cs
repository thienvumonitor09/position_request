﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{
    private static readonly string strConnectionLocal = "Data Source=localhost;Workstation ID=internalEAN;Initial Catalog=PR;Integrated Security=true;";
    private static readonly string strConnectionLocal2 = "Data Source=localhost;Workstation ID=internalEAN;Initial Catalog=PR2;Integrated Security=true;";

    /*
    public class Position
    {
        private string officialTitle;
        private string workingTitle;

        public string OfficialTitle { get => officialTitle; set => officialTitle = value; }
        public string WorkingTitle { get => workingTitle; set => workingTitle = value; }
    }
    */
    /*
    public class Employee
    {
        public string empId;
        public string empName;
        public string deptName;

        public Employee(string empId, string empName, string deptName)
        {
            this.empId = empId;
            this.empName = empName;
            this.deptName = deptName;
        }*/

    /*
    public string EmpId { get ; set; }
    public string EmpName { get; set; }
    public string DeptName { get; set; }*/
    /*
public Employee(string empId, string empName, string deptName)
{
this.empId = empId;
this.empName = empName;
this.deptName = deptName;
}*/

    /*
        }

    */

    /*
public class Department
{
    public string departmentName;
    public string departmentNo;
    public string unit;
    public Department(string departmentName, string departmentNo, string unit)
    {
        this.departmentName = departmentName;
        this.departmentNo = departmentNo;
        this.unit = unit;
    }
}
*/
    public class NewReplace
    {
        public int NewReplaceID { get; set; }
        public string PositionID { get; set; }
    }
    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [System.Web.Services.WebMethod()]
    public void GetDepartmentList()
    {
        List<Department> departments = new List<Department>();
        DataSet ds = new DataSet();
        //string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSEAN;Integrated Security=true;";
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        string query = @"SELECT DISTINCT [DESCR], DeptTable.[DEPTID], [BUSINESS_UNIT] 
                        FROM[ctcLink_ODS].[dbo].[DepartmentTable_HC] AS DeptTable 
                        INNER JOIN[ctcLink_ODS].[dbo].[Job] AS EmployeeJob 
                        ON EmployeeJob.DEPTID = DeptTable.DEPTID 
                        WHERE DeptTable.[DEPTID] > '98500' AND DeptTable.[DEPTID] <> 'ALL_DEPT' 
                        ORDER BY [DESCR]";

        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        //var result = "";
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //employees.Add(new Employee(int.Parse(dr["EMPLID"].ToString()), dr["Name"].ToString()));
                departments.Add(new Department(dr["DESCR"].ToString(), dr["DEPTID"].ToString(), dr["BUSINESS_UNIT"].ToString()));
                //result += dr["Name"].ToString();
            }
        }
        conn.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        Context.Response.Write(js.Serialize(departments));
    }
    [System.Web.Services.WebMethod()]
    public string GetEmpList()
    {

        List<Employee> employees = new List<Employee>();
        DataSet ds = new DataSet();
        //string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSEAN;Integrated Security=true;";
        SqlConnection myConnection = new SqlConnection(strConnectionLocal);
        myConnection.Open();
        string query = @"WITH maxTbl (EmplId, empName, maxEFDT )  
                        AS  
                        (  
                            SELECT  EmplId, name as empName, MAX(EFFDT) as maxEFDT 
                            FROM[ctcLink_ODS].[dbo].[Names_HC] AS EmployeeNames 
                            GROUP BY EmployeeNames.EmplId, name 
                        )  
                        ,
                        tmp (EmplId, EmpName, DEPTID )
                        AS 
                        (
	                         SELECT maxTbl.EmplId, EmpName, DEPTID 
                             FROM maxTbl 
	                         INNER JOIN [ctcLink_ODS].[dbo].[Job] AS EmployeeJob ON maxTbl.EmplId = EmployeeJob.EmplID 
                        )

                        SELECT Distinct EmplId,EmpName, tmp.DEPTID, Descr AS DeptName 
                        FROM tmp 
                        LEFT OUTER JOIN [ctcLink_ODS].[dbo].[DepartmentTable_HC] AS DeptTable ON DeptTable.DeptID = tmp.DeptID 
                        ORDER BY EmpName";

        SqlCommand cmd = new SqlCommand(query, myConnection);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        //var result = "";
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //employees.Add(new Employee(int.Parse(dr["EMPLID"].ToString()), dr["Name"].ToString()));
                employees.Add(new Employee(dr["EMPLID"].ToString(), dr["EmpName"].ToString(), dr["DeptName"].ToString() ));
                //employees.Add(new Employee() { EmpId = dr["EMPLID"].ToString(), EmpName = dr["EmpName"].ToString(), DeptName = dr["DeptName"].ToString() });
                //result += dr["Name"].ToString();
            }
        }
        myConnection.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        js.MaxJsonLength = Int32.MaxValue;
        //Context.Response.Write(js.Serialize(employees));
        return js.Serialize(employees);
    }

    [WebMethod()]
    public string SaveEmpList(string name)
    {
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        string sqlins = @"INSERT INTO [PR].[dbo].[Employee]  (Name) values (@Name)";
        SqlCommand cmd = new SqlCommand(sqlins, conn);
        // create your parameters and add Value
        cmd.Parameters.AddWithValue("@Name", name);
        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
            //Console.WriteLine("Connection Closed.");
        }
        return name;   
    }

    [WebMethod()]
    public string SavePosition(string positionById)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        Position position = new Position();
        var deserializedResult = js.Deserialize<Position>(positionById);
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        if (String.IsNullOrEmpty(deserializedResult.PositionID))
        {
            //If  PositionID not found, add new
            string sqlins = @"INSERT INTO [PR].[dbo].[Position]  (officialTitle, workingTitle) values (@officialTitle, @workingTitle);
                            SELECT CAST(SCOPE_IDENTITY() AS INT) "; //scope_identity() used to retrieve inserted id
            SqlCommand cmd = new SqlCommand(sqlins, conn);
            // create your parameters and add Value
            cmd.Parameters.AddWithValue("@officialTitle", deserializedResult.OfficialTitle);
            cmd.Parameters.AddWithValue("@workingTitle", String.IsNullOrEmpty(deserializedResult.WorkingTitle) ? (object)DBNull.Value : deserializedResult.WorkingTitle); //null cannot be saved to db, instead DBNull.Value
            //deserializedResult.PositionID = "10";
            try
            {
                conn.Open();
                //cmd.ExecuteNonQuery();
                deserializedResult.PositionID = cmd.ExecuteScalar().ToString(); //Executes the query, and returns the first column of the first row in the result set returned by the query.Additional columns or rows are ignored.
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            //If  PositionID found, update existing
            string sqlUpdate = @" UPDATE [PR].[dbo].[Position] 
                                    SET [officialTitle] = @OfficialTitle , [workingTitle] = @WorkingTitle
                                    WHERE ID = "+ int.Parse(deserializedResult.PositionID);
            SqlCommand cmd = new SqlCommand(sqlUpdate, conn);
            // create your parameters
            cmd.Parameters.Add("@OfficialTitle", System.Data.SqlDbType.VarChar);
            cmd.Parameters.Add("@WorkingTitle", System.Data.SqlDbType.VarChar);
            // set values to parameters from textboxes
            cmd.Parameters["@OfficialTitle"].Value = deserializedResult.OfficialTitle;
            cmd.Parameters["@WorkingTitle"].Value = deserializedResult.WorkingTitle;
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }


        }
        return GetPositionById(deserializedResult.PositionID);
        //return js.Serialize(deserializedResult);
        //return deserializedResult.PositionID;
    }

    [WebMethod()]
    public string SavePositionAll(string positionById)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        Position position = new Position();
        var deserializedResult = js.Deserialize<Position>(positionById);
        SqlConnection conn = new SqlConnection(strConnectionLocal2);
        if (String.IsNullOrEmpty(deserializedResult.PositionID))
        {
            //If  PositionID not found, add new
            string sqlins = @"INSERT INTO [PR2].[dbo].[Position]
                                   (
                                    [OfficialTitle]
                                   ,[WorkingTitle]
                                   ,[College]
                                   ,[ClassNo]
                                   ,[BudgetNo]
                                   ,[PositionControlNo]
                                   ,[Department]
                                   ,[DepartmentNo]
                                   ,[PhoneNo]
                                   ,[MailStop]
                                   ,[Supervisor]
                                   ,[SupervisorID]
                                   ,[RecruitmentType]
                                   ,[PositionType]
                                   ,[NewReplace]
                                   ,[NameReplace]
                                   ,[OrginatorID])
                             VALUES
                                   (
                                    @OfficialTitle
                                   ,@WorkingTitle
                                   ,@College
                                   ,@ClassNo
                                   ,@BudgetNo
                                   ,@PositionControlNo
                                   ,@Department
                                   ,@DepartmentNo
                                   ,@PhoneNo
                                   ,@MailStop
                                   ,@Supervisor
                                   ,@SupervisorID
                                   ,@RecruitmentType
                                   ,@PositionType
                                   ,@NewReplace
                                   ,@NameReplace
                                   ,@OrginatorID);
                            SELECT CAST(SCOPE_IDENTITY() AS INT) "; //scope_identity() used to retrieve inserted id
            SqlCommand cmd = new SqlCommand(sqlins, conn);
            // create your parameters and add Value
            cmd.Parameters.AddWithValue("@OfficialTitle", deserializedResult.OfficialTitle);
            cmd.Parameters.AddWithValue("@WorkingTitle", String.IsNullOrEmpty(deserializedResult.WorkingTitle) ? (object)DBNull.Value : deserializedResult.WorkingTitle); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@College", String.IsNullOrEmpty(deserializedResult.College) ? (object)DBNull.Value : deserializedResult.College); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@ClassNo", String.IsNullOrEmpty(deserializedResult.ClassNo) ? (object)DBNull.Value : deserializedResult.ClassNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@BudgetNo", String.IsNullOrEmpty(deserializedResult.BudgetNo) ? (object)DBNull.Value : deserializedResult.BudgetNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@PositionControlNo", String.IsNullOrEmpty(deserializedResult.PositionControlNo) ? (object)DBNull.Value : deserializedResult.PositionControlNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@Department", String.IsNullOrEmpty(deserializedResult.Department) ? (object)DBNull.Value : deserializedResult.Department); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@DepartmentNo", String.IsNullOrEmpty(deserializedResult.DepartmentNo) ? (object)DBNull.Value : deserializedResult.DepartmentNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@PhoneNo", String.IsNullOrEmpty(deserializedResult.PhoneNo) ? (object)DBNull.Value : deserializedResult.PhoneNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@MailStop", String.IsNullOrEmpty(deserializedResult.MailStop) ? (object)DBNull.Value : deserializedResult.MailStop); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@Supervisor", String.IsNullOrEmpty(deserializedResult.Supervisor) ? (object)DBNull.Value : deserializedResult.Supervisor); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@SupervisorID", String.IsNullOrEmpty(deserializedResult.SupervisorID) ? (object)DBNull.Value : deserializedResult.SupervisorID); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@RecruitmentType", String.IsNullOrEmpty(deserializedResult.RecruitmentType) ? (object)DBNull.Value : deserializedResult.RecruitmentType); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@PositionType", String.IsNullOrEmpty(deserializedResult.PositionType) ? (object)DBNull.Value : deserializedResult.PositionType); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@NewReplace", String.IsNullOrEmpty(deserializedResult.NewReplace) ? (object)DBNull.Value : deserializedResult.NewReplace); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@NameReplace", String.IsNullOrEmpty(deserializedResult.NameReplace) ? (object)DBNull.Value : deserializedResult.NameReplace); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@OrginatorID", "101009742");
            //deserializedResult.PositionID = "10";
            try
            {
                conn.Open();
                //cmd.ExecuteNonQuery();
                deserializedResult.PositionID = cmd.ExecuteScalar().ToString(); //Executes the query, and returns the first column of the first row in the result set returned by the query.Additional columns or rows are ignored.
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            //If  PositionID found, update existing
            string sqlUpdate = @" UPDATE [PR2].[dbo].[Position] 
                                    SET [OfficialTitle] = @OfficialTitle
                                      ,[WorkingTitle] = @WorkingTitle
                                      ,[College] = @College
                                      ,[ClassNo] = @ClassNo
                                      ,[BudgetNo] = @BudgetNo
                                      ,[PositionControlNo] = @PositionControlNo
                                      ,[Department] = @Department
                                      ,[DepartmentNo] = @DepartmentNo
                                      ,[PhoneNo] = @PhoneNo
                                      ,[MailStop] = @MailStop
                                      ,[Supervisor] = @Supervisor
                                      ,[SupervisorID] = @SupervisorID
                                      ,[RecruitmentType] = @RecruitmentType
                                      ,[PositionType] = @PositionType
                                      ,[NewReplace] = @NewReplace
                                      ,[NameReplace] = @NameReplace
                                    WHERE PositionID = " + int.Parse(deserializedResult.PositionID);
            SqlCommand cmd = new SqlCommand(sqlUpdate, conn);
            // create your parameters and add Value
            cmd.Parameters.AddWithValue("@OfficialTitle", deserializedResult.OfficialTitle);
            cmd.Parameters.AddWithValue("@WorkingTitle", String.IsNullOrEmpty(deserializedResult.WorkingTitle) ? (object)DBNull.Value : deserializedResult.WorkingTitle); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@College", String.IsNullOrEmpty(deserializedResult.College) ? (object)DBNull.Value : deserializedResult.College); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@ClassNo", String.IsNullOrEmpty(deserializedResult.ClassNo) ? (object)DBNull.Value : deserializedResult.ClassNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@BudgetNo", String.IsNullOrEmpty(deserializedResult.BudgetNo) ? (object)DBNull.Value : deserializedResult.BudgetNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@PositionControlNo", String.IsNullOrEmpty(deserializedResult.PositionControlNo) ? (object)DBNull.Value : deserializedResult.PositionControlNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@Department", String.IsNullOrEmpty(deserializedResult.Department) ? (object)DBNull.Value : deserializedResult.Department); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@DepartmentNo", String.IsNullOrEmpty(deserializedResult.DepartmentNo) ? (object)DBNull.Value : deserializedResult.DepartmentNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@PhoneNo", String.IsNullOrEmpty(deserializedResult.PhoneNo) ? (object)DBNull.Value : deserializedResult.PhoneNo); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@MailStop", String.IsNullOrEmpty(deserializedResult.MailStop) ? (object)DBNull.Value : deserializedResult.MailStop); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@Supervisor", String.IsNullOrEmpty(deserializedResult.Supervisor) ? (object)DBNull.Value : deserializedResult.Supervisor); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@SupervisorID", String.IsNullOrEmpty(deserializedResult.SupervisorID) ? (object)DBNull.Value : deserializedResult.SupervisorID); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@RecruitmentType", String.IsNullOrEmpty(deserializedResult.RecruitmentType) ? (object)DBNull.Value : deserializedResult.RecruitmentType); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@PositionType", String.IsNullOrEmpty(deserializedResult.PositionType) ? (object)DBNull.Value : deserializedResult.PositionType); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@NewReplace", String.IsNullOrEmpty(deserializedResult.NewReplace) ? (object)DBNull.Value : deserializedResult.NewReplace); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@NameReplace", String.IsNullOrEmpty(deserializedResult.NameReplace) ? (object)DBNull.Value : deserializedResult.NameReplace); //null cannot be saved to db, instead DBNull.Value
            cmd.Parameters.AddWithValue("@OrginatorID", "101009742");
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }


        }
        return GetPositionAllById(deserializedResult.PositionID);
        //return js.Serialize(deserializedResult);
        //return deserializedResult.PositionID;

    }

    [WebMethod()]
    public string GetCurrentTime(string name, string name2)
    {
        return name + " " + name2;
    }

    [WebMethod()]
    public string UploadFile()
    {
        string sPath = "";
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/");
        var postedFile = System.Web.HttpContext.Current.Request.Files["file"];
        if (postedFile != null)
        {
            
            var filePath = HttpContext.Current.Server.MapPath("~/UploadFiles/" + postedFile.FileName);
            postedFile.SaveAs(filePath);
            return filePath;
        }
        else
        {
            return "Error";
        }

        //return "Error";
        //System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
        /*
        // CHECK THE FILE COUNT.
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];

            if (hpf.ContentLength > 0)
            {
                // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                {
                    // SAVE THE FILES IN THE FOLDER.
                    hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                    iUploadedCnt = iUploadedCnt + 1;
                }
            }
        }*/
        //return sPath;
    }

    [WebMethod()]
    public string GetPositionById(string id)
    {
        Position position = new Position();
        DataSet ds = new DataSet();
        //string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSEAN;Integrated Security=true;";
        SqlConnection conn = new SqlConnection(strConnectionLocal);
        conn.Open();
        string query = "SELECT * FROM [PR].[dbo].[Position] WHERE id =" + id;

        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        //var result = "";
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //employees.Add(new Employee(int.Parse(dr["EMPLID"].ToString()), dr["Name"].ToString()));
                position.PositionID = dr["id"].ToString();
                position.OfficialTitle = dr["officialTitle"].ToString();
                position.WorkingTitle = dr["workingTitle"].ToString();
                //result += dr["Name"].ToString();
            }
        }
        conn.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        //Context.Response.Write(js.Serialize(position));
        return js.Serialize(position);
    }
    [WebMethod()]
    public string GetPositionAllById(string id)
    {
        Position position = new Position();
        DataSet ds = new DataSet();
        //string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSEAN;Integrated Security=true;";
        //string strConnectionLocal2 = "Data Source=localhost;Workstation ID=internalEAN;Initial Catalog=PR2;Integrated Security=true;";
        SqlConnection conn = new SqlConnection(strConnectionLocal2);
        conn.Open();
        string query = "SELECT * FROM [PR2].[dbo].[Position] WHERE PositionID =" + id;

        SqlCommand cmd = new SqlCommand(query, conn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        //var result = "";
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //employees.Add(new Employee(int.Parse(dr["EMPLID"].ToString()), dr["Name"].ToString()));
                position.PositionID = dr["PositionID"].ToString();
                position.OfficialTitle = dr["OfficialTitle"].ToString();
                position.WorkingTitle = dr["WorkingTitle"].ToString();
                position.College = dr["College"].ToString();
                position.ClassNo = dr["ClassNo"].ToString();
                position.BudgetNo = dr["BudgetNo"].ToString();
                position.PositionControlNo = dr["PositionControlNo"].ToString();
                position.Department = dr["Department"].ToString();
                position.DepartmentNo = dr["DepartmentNo"].ToString();
                position.PhoneNo = dr["PhoneNo"].ToString();
                position.MailStop = dr["MailStop"].ToString();
                position.Supervisor = dr["Supervisor"].ToString();
                position.SupervisorID = dr["SupervisorID"].ToString();
                position.RecruitmentType = dr["RecruitmentType"].ToString();
                position.PositionType = dr["PositionType"].ToString();
                position.NewReplace = dr["NewReplace"].ToString();
                position.NameReplace = dr["NameReplace"].ToString();
                position.OrginatorID = dr["OrginatorID"].ToString();
                //result += dr["Name"].ToString();
            }
        }
        conn.Close();
        JavaScriptSerializer js = new JavaScriptSerializer();
        //Context.Response.Write(js.Serialize(position));
        return js.Serialize(position);
    }

    [WebMethod()]
    public void SaveNewReplace(string newReplace)
    {
        JavaScriptSerializer js = new JavaScriptSerializer();
        NewReplace newReplaceObj = new NewReplace ();
        var deserializedResult = js.Deserialize<NewReplace>(newReplace);
        SqlConnection conn = new SqlConnection(strConnectionLocal2);
        //If  PositionID not found, add new
        string sqlins = @"INSERT INTO [PR2].[dbo].[NewReplace] (PositionID) VALUES ( @PositionID);
                            SELECT CAST(SCOPE_IDENTITY() AS INT) "; //scope_identity() used to retrieve inserted id
        SqlCommand cmd = new SqlCommand(sqlins, conn);
        // create your parameters and add Value
        cmd.Parameters.AddWithValue("@PositionID", "2");
        
        //deserializedResult.PositionID = "10";
        try
        {
            conn.Open();
            //cmd.ExecuteNonQuery();
            deserializedResult.PositionID = cmd.ExecuteScalar().ToString(); //Executes the query, and returns the first column of the first row in the result set returned by the query.Additional columns or rows are ignored.
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Close();
        }
        //return newReplace;
        //return js.Serialize(newReplace);
    }
}
