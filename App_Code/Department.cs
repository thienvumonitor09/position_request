﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Department
/// </summary>
public class Department
{
    public string departmentName;
    public string departmentNo;
    public string unit;
    public Department(string departmentName, string departmentNo, string unit)
    {
        this.departmentName = departmentName;
        this.departmentNo = departmentNo;
        this.unit = unit;
    }
}