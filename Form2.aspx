﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="Form2.aspx.cs" Inherits="Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
    <script type="text/javascript" src="https://internal.spokane.edu/jquery/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        li {
            cursor: pointer;
        }

         li:hover {
            background-color: #f9f9f9;
         }
          
        
    </style>
    <script type="text/javascript">
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function ($scope, $http) {
            //$scope.names = ["Email", "Tobias", "Linus"];
            $scope.units = ["District", "SCC", "SFCC"];
            $scope.departments = ["ISDS", "Marketing", "HR"];
            $scope.positions = ["Classified", "Part-time hourly", "Tenure-track faculty", "Non tenure-track faculty", "Adjunct faculty",
                "Professional/confidential exempt", "Administrator/executives"];
            $scope.recruitmentTypes = ["Promotional", "Internal", "Open_competive"];
            

            $scope.populateSchedule = function () {
                $scope.from2 = $scope.from1;
                $scope.from3 = $scope.from1;
                $scope.from4 = $scope.from1;
                $scope.from5 = $scope.from1;
                $scope.from6 = $scope.from1;
                $scope.from7 = $scope.from1;

                $scope.to2 = $scope.to1;
                $scope.to3 = $scope.to1;
                $scope.to4 = $scope.to1;
                $scope.to5 = $scope.to1;
                $scope.to6 = $scope.to1;
                $scope.to7 = $scope.to1;
            };

            $scope.resetSchedule = function () {
                $scope.from1 = "";
                $scope.from2 = "";
                $scope.from3 = "";
                $scope.from4 = "";
                $scope.from5 = "";
                $scope.from6 = "";
                $scope.from7 = "";
                $scope.to1= "";
                $scope.to2 = "";
                $scope.to3 = "";
                $scope.to4 = "";
                $scope.to5 = "";
                $scope.to6 = "";
                $scope.to7 = "";
            }

            $scope.employeeList = [];
            //$scope.employeeList = ["Nelson,Bob", "Nguyen,Vu", "Vaughn,Brandy R", "Padden,Laura", "Anderson,Jeremy L", "Casey,Carolyn J"];
            $scope.complete = function (string) {
                $scope.hidethis = false;
                var output = [];
                angular.forEach($scope.employeeList, function (value, key) {
                    if (value.name.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                        output.push(value);
                    }
                });
                $scope.filterEmp = output;
                if (string == ""){
                    $scope.hidethis = true;
                }
            }
            $scope.fillTextbox = function (string) {
                $scope.searchName = string.name;
                $scope.empId = string.empId;
                $scope.hidethis = true;
            }
            
            $scope.fillList = function () {
                $http({
                    method: 'POST',
                    url: 'WebService.asmx/GetList',
                    dataType: 'json',
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then(function (response) {
                    //alert(JSON.stringify(response.data));
                    //console.log(JSON.stringify(response));
                    $scope.employeeList = response.data;
                }, function (error) {
                    alert("error");
                });   
            };
            $scope.fillList();
            /*
            $scope.StudentList = [
                { StudentID: "Ford Mustang", StudentName: "red" },
                { StudentID: "Fiat 500", StudentName: "white" },
                { StudentID: "Volvo XC90", StudentName: "black" }
            ];
            */

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" Runat="Server">
 <div ng-app="myApp" ng-controller="myCtrl">
    <!-- 
     
   <div class="dropdown">
        <input type="text" name="country" id="country" ng-model="searchName" ng-keyup="complete(searchName)" class="form-control" />
        <ul class="list-group" ng-model="hidethis" ng-hide="hidethis">
            <li class="list-group-item" ng-repeat="employee in filterEmp" ng-click="fillTextbox(employee)">{{employee.name}} {{employee.empId}}</li>
        </ul>
   </div>
           EMPID:{{empId}} -->
    <!-- <form id="frmPR" runat="server" enctype="multipart/form-data"> -->
     <!--
     <input type="text" class="form-control " placeholder="Search employees" ng-model="searchText" /> 
     <table>

     <tr ng-repeat="student in StudentList | filter:searchText">
                <td ng-bind="student.StudentID">
                </td>
                <td ng-bind="student.StudentName">
                </td>
            </tr>
     </table>
     -->
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>

        <h3 class="pageTitle">Position Request (PR)</h3>

        <!-- SEARCH AND LIST POSITION REQUEST FORMS -->
        <asp:Panel ID="panList" runat="server">
            
        </asp:Panel>

        <!-- ADD POSITION REQUEST FORM -->
        <asp:Panel ID="panAdd" runat="server">
            
           <asp:Label ID="lblUploadError1" runat="server" style="color:Red"></asp:Label>
            <div class="title">
                ORIGINATOR:&nbsp;<span class="normal"><asp:Label ID="lblOrigName1" runat="Server"></asp:Label></span>&nbsp;ctcLink ID:&nbsp;<span class="normal"><asp:Label ID="lblOrigSID1" runat="Server"></asp:Label></span>
            </div>
            <div class="section">
               <!--
                <p>Name : <input type="text" ng-model="name"></p>
                <h1>Hello {{name}}</h1>
           
                <select>
                    <option ng-repeat="x in names">{{x}}</option>
                </select>
            -->
                <!-- Official position title-->
                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:100%;float:left;">
                        <strong>Official position title*</strong><br />
                        <asp:TextBox ID="TextBox1" runat="Server" MaxLength="30" style="width:100%"></asp:TextBox>
                    </div>
                    <div class="clearer"></div>
                </div>

                <!-- Working title-->
                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:70%;float:left;">
                        <strong>Working title*</strong><br />
                        <asp:TextBox ID="TextBox2" runat="Server" MaxLength="30" style="width:80%"></asp:TextBox>
                    </div>
                     <div style="width:30%;float:left;">
                        <strong>Class #*</strong><br />
                        <asp:TextBox ID="TextBox4" runat="Server" MaxLength="30" style="width:100%"></asp:TextBox>
                    </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:30%;float:left;">
                        <strong>College/Unit *</strong><br />
                            <select>
                                <option ng-repeat="x in units">{{x}}</option>
                            </select>
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Budget #*</strong><br />
                        <asp:TextBox ID="TextBox3" runat="Server" MaxLength="30" style="width:90%"></asp:TextBox>
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Position control #</strong><br />
                        <asp:TextBox ID="TextBox5" runat="Server" MaxLength="30" style="width:100%"></asp:TextBox>
                    </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:0px 0px 5px 5px;">
                    <div style="width:70%;float:left;">
                        <strong>Department Name*</strong><br />
                        <select ng-model="department">
                                <option ng-repeat="x in departments">{{x}}</option>
                         </select>
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Department #</strong>
                        <div style="padding-top:3px">{{department}}</div>
                    </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:70%;float:left;">
                        <strong>Position phone # *</strong><br />
                        <asp:TextBox ID="TextBox6" runat="Server" MaxLength="30" style="width:90%"></asp:TextBox>
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Mail stop*</strong><br />
                        <asp:TextBox ID="TextBox7" runat="server" MaxLength="30" style="width:100%"></asp:TextBox>
                    </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:70%;float:left;">
                        <strong>Immediate Supervisor</strong><br />
                        <!-- <asp:TextBox ID="txtOrigSuperName1" runat="Server" MaxLength="30" style="width:90%"></asp:TextBox>-->
                        <div class="dropdown">
                            <input type="text" name="country" id="country" ng-model="searchName" ng-keyup="complete(searchName)" class="form-control" />
                            <ul class="list-group" ng-model="hidethis" ng-hide="hidethis">
                                <li class="list-group-item" ng-repeat="employee in filterEmp" ng-click="fillTextbox(employee)">{{employee.name}} {{employee.empId}}</li>
                            </ul>
                         </div>
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Supervisor's EMPID</strong><br />
                        <!--<asp:TextBox ID="txtOrigSuperPh1" runat="server" MaxLength="30" style="width:100%"></asp:TextBox> -->{{empId}}
                    </div>
                    <div class="clearer"></div>
                </div>

            </div> 

            <div class="title">Type of position </div>
            <div class="section">
                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:50%;float:left;">
                        <strong>Type of position *</strong><br />
                            <select ng-model="positionType">
                                <option ng-repeat="x in positions">{{x}}</option>
                         </select>
                    </div>
                    <div style="width:50%;float:left;" id="recruitmentType">
                        <strong>Type of recruitment</strong><br />
                        <select ng-model="recruitmentType">
                            <option ng-repeat="x in recruitmentTypes">{{x}}</option>
                         </select>
                    </div>
                    
                    <div class="clearer"></div>
                </div>

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:50%;float:left;">
                        <div style="width:30%;float:left;">
                            <input type="radio" id="new" ng-model="newReplace" value="new" />New <br />
                            <input type="radio" id="replace" ng-model="newReplace" value="replace" />Replace
                        </div>
                        <div style="width:70%;float:left;" ng-show="newReplace == 'replace'">
                            <strong>Name of employee replaced</strong><br />
                        <asp:TextBox ID="replacedEmployee" runat="server" MaxLength="30" style="width:95%"></asp:TextBox>
                        </div>
                    </div>



                    <div style="width:50%;float:left;"  ID="classifiedType" ng-show="positionType == 'Classified' || positionType == 'Part-time hourly'">
                        <div style="width:50%;float:left;">
                             <input type="radio" ng-model="permanent" value="permanent">Permanent <br />
                            <input type="radio" ng-model="permanent" value="nonPermanent">Non-permanent
                        </div>
                        <div style="width:50%;float:left;" ng-show="permanent == 'nonPermanent'">
                            <strong>End Date</strong><br />
                            <asp:TextBox ID="endDate" runat="server" MaxLength="30" style="width:100%"></asp:TextBox>
                        </div>
                    </div>
                    
                    

                    
                    <div class="clearer"></div>
                </div>
                <!-- <div ng-show="positionType == 'Classified' || positionType == 'Part-time hourly'"> -->
                <div>
                    <strong>Position work schedule</strong>
                    <div style="padding:5px 0px 5px 5px;">
                        <div style="width:30%;float:left;">
                                <strong># of hours per day</strong><br />
                                <asp:TextBox ID="TextBox8" runat="server" MaxLength="30" style="width:90%"></asp:TextBox>
                            </div>
                            <div style="width:30%;float:left;">
                                <strong># of hours per week</strong><br />
                                <asp:TextBox ID="TextBox11" runat="server" MaxLength="30" style="width:90%"></asp:TextBox>
                            </div>
                            <div style="width:40%;float:left;">
                                <strong># of months per year</strong><br />
                                    <input type="text" ng-model="monthYr">
                                <p ng-show ="monthYr < 12">
                                    <strong>Cyclic Calendar Code</strong><br />
                                    <asp:TextBox ID="TextBox14" runat="server" MaxLength="30" style="width:100%"></asp:TextBox>
                                 </p>
                            </div>
                    
                            <div class="clearer"></div>
                    </div>
                   
                    <button ng-click="populateSchedule()">Apply All</button>
                    <button ng-click="resetSchedule()">Reset</button>
                    <div style="padding:5px 0px 5px 5px;">
                            <div style="width:100%;float:left;">
                                <div style="width:20%;float:left;">Monday</div>
                                <div style="width:40%;float:left;">
                                     <input type="text" placeholder="From" ng-model="from1"><br>
                                </div>
                                <div style="width:40%;float:left;">
                                    <input type="text" placeholder="To" ng-model ="to1"><br>
                                </div>
                            </div>
                            <div style="width:100%;float:left;">
                                 <div style="width:20%;float:left;" >Tuesday</div>
                                <div style="width:40%;float:left;">
                                     <input type="text" placeholder="From" ng-model="from2"><br>
                                </div>
                                <div style="width:40%;float:left;">
                                    <input type="text" placeholder="To" ng-model ="to2"><br>
                                </div>
                            </div>
                            <div style="width:100%;float:left;">
                                    <div style="width:20%;float:left;">Wednesday</div>
                                <div style="width:40%;float:left;">
                                     <input type="text" placeholder="From" ng-value="from3"><br>
                                </div>
                                <div style="width:40%;float:left;">
                                    <input type="text" placeholder="To" ng-value ="to3"><br>
                                </div>
                                </div>
                            <div style="width:100%;float:left;">
                                    <div style="width:20%;float:left;">Thursday</div>
                                <div style="width:40%;float:left;">
                                     <input type="text" placeholder="From" ng-value="from4"><br>
                                </div>
                                <div style="width:40%;float:left;">
                                    <input type="text" placeholder="To" ng-value ="to4"><br>
                                </div>
                                </div>
                            <div style="width:100%;float:left;">
                                    <div style="width:20%;float:left;">Friday</div>
                                <div style="width:40%;float:left;">
                                     <input type="text" placeholder="From" ng-value="from5"><br>
                                </div>
                                <div style="width:40%;float:left;">
                                    <input type="text" placeholder="To" ng-value ="to5"><br>
                                </div>
                                </div>
                            <div style="width:100%;float:left;">
                                    <div style="width:20%;float:left;">Saturday</div>
                                <div style="width:40%;float:left;">
                                     <input type="text" placeholder="From" ng-value="from6"><br>
                                </div>
                                <div style="width:40%;float:left;">
                                    <input type="text" placeholder="To" ng-value ="to6"><br>
                                </div>
                                </div>
                            <div style="width:100%;float:left;">
                                    <div style="width:20%;float:left;">Sunday</div>
                                <div style="width:40%;float:left;">
                                     <input type="text" placeholder="From" ng-value="from7"><br>
                                </div>
                                <div style="width:40%;float:left;">
                                    <input type="text" placeholder="To" ng-value ="to7"><br>
                                </div>
                                </div>
                            <div class="clearer"></div>
                    </div>
                </div>

            </div>
            
           
        </asp:Panel>

        <!-- EDIT POSITION REQUEST FORM -->
        <asp:Panel ID="panEdit" runat="server">
            
        </asp:Panel>

        <!-- VIEW POSITION REQUEST FORM -->
        <asp:Panel ID="panView" runat="server">
            <!-- view originator PR form fields go here -->
        </asp:Panel>

        <!-- CONFIRM SAVED / SUBMITTED POSITION REQUEST FORM -->
        <asp:Panel ID="panConfirm" runat="server">

        </asp:Panel>

        <!-- DISPLAY ERROR MESSAGE -->
        <asp:Panel ID="panError" runat="server"></asp:Panel>
    <!--</form> -->
</div>
</asp:Content>

