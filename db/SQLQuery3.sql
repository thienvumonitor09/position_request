USE PR2 ; 
DROP TABLE IF EXISTS PositionOnBoarding;
DROP TABLE IF EXISTS Onboarding;
DROP TABLE IF EXISTS Schedule;
DROP TABLE IF EXISTS Position;
DROP TABLE IF EXISTS Originator;



CREATE TABLE Originator
( OriginatorID VARCHAR(50) NOT NULL PRIMARY KEY,
  OriginatorName VARCHAR(50) NOT NULL
);
INSERT INTO [dbo].[Originator]
           ([OriginatorID]
           ,[OriginatorName])
     VALUES
           ('101009742'
           ,'Nguyen,Vu K')
;
CREATE TABLE Position
( PositionID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  OfficialTitle VARCHAR(50) NULL,
  WorkingTitle VARCHAR(50) NOT NULL,
  College VARCHAR(50) NOT NULL,
  ClassNo VARCHAR(50) NOT NULL,
  BudgetNo VARCHAR(50) NOT NULL,
  PositionControlNo VARCHAR(50) NULL,
  Department VARCHAR(100) NOT NULL,
  DepartmentNo VARCHAR(50) NOT NULL,
  PhoneNo VARCHAR(50) NOT NULL,
  MailStop VARCHAR(50) NOT NULL,
  Supervisor VARCHAR(500) NOT NULL,
  SupervisorID VARCHAR(50) NOT NULL,
  RecruitmentType VARCHAR(50) NOT NULL,
  PositionType VARCHAR(50) NOT NULL,
  NewReplace VARCHAR(50) NOT NULL,
  NameReplace VARCHAR(500) NULL,
  OrginatorID VARCHAR(50) NOT NULL,
  CONSTRAINT FK_Position
		FOREIGN KEY (OrginatorID)
		REFERENCES Originator (OriginatorID)	
		ON DELETE CASCADE    
		ON UPDATE CASCADE 
);



CREATE TABLE Schedule
(
	ScheduleID INT NOT NULL IDENTITY(1,1),
	PositionID INT NOT NULL, 
	DayHOUR INT NOT NULL, 
	WeekHour INT NOT NULL, 
	MonthYear INT NOT NULL, 
	CONSTRAINT PK_Schedule PRIMARY KEY (ScheduleID,PositionID),
	CONSTRAINT FK_Schedule
		FOREIGN KEY (PositionID)
		REFERENCES Position (PositionID)	 
		ON DELETE CASCADE    
		ON UPDATE CASCADE 
);



CREATE TABLE Onboarding
(
	OnboardingID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(100) NOT NULL
);

CREATE TABLE PositionOnboarding
(
	OnboardingID INT NOT NULL,
	PositionID INT NOT NULL, 
	CONSTRAINT PK_PositionOnboarding PRIMARY KEY (OnboardingID,PositionID),
	CONSTRAINT FK_BoardingID
		FOREIGN KEY (OnboardingID)
		REFERENCES Onboarding (OnboardingID)	
		ON DELETE CASCADE    
		ON UPDATE CASCADE,
	CONSTRAINT FK_PositionID
		FOREIGN KEY (PositionID)
		REFERENCES Position (PositionID)	
		ON DELETE CASCADE    
		ON UPDATE CASCADE,	 
);
