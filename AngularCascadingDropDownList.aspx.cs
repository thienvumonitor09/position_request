﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

[System.Web.Script.Services.ScriptService]
public partial class AngularCascadingDropDownList : System.Web.UI.Page
{
    public class Names
    {
        public int StudentID;
        public string StudentName;
        public Names(int _StudentID, string _StudentName)
        {
            StudentID = _StudentID;
            StudentName = _StudentName;
        }
    }
  
    [System.Web.Services.WebMethod()]
    public void GetList()
    {
        
        List<Names> names = new List<Names>();
        DataSet ds = new DataSet();
        string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSEAN;Integrated Security=true;";
        SqlConnection myConnection = new SqlConnection(strConnection);
        myConnection.Open();
        string query = "SELECT TOP (10) [EMPLID],[Name] FROM [ctcLink_ODS].[dbo].[Names_HC]";

        SqlCommand cmd = new SqlCommand(query, myConnection);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        var result = "";
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                names.Add(new Names(int.Parse(dr["EMPLID"].ToString()), dr["Name"].ToString()));
                result += dr["Name"].ToString();
            }
        }
        JavaScriptSerializer js = new JavaScriptSerializer();
        Context.Response.Write(names);
        //return names;
        //var jsondata = new JavaScriptSerializer().Serialize(names);
        // return result;
        //return jsondata;
        //return names;
        
    }

    
}