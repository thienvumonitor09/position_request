﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="Form3.aspx.cs" Inherits="Form3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        li {
            cursor: pointer;
        }

         li:hover {
            background-color: #aeaeae;
         }
         .dropdown {
            position: relative;
            display: inline-block;
        }
          .dropdown:hover {
            position: absolute;
           }
        
    </style>
    <script type="text/javascript" src="Scripts/app.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" Runat="Server">
<div ng-app="myApp" ng-controller="myCtrl">
   
    
     <form id="mainForm" name="myForm" novalidate>
         Name:
    <input type="text" ng-model="Name" />
    <br />
    <input type="button" value="Submit" ng-click="ButtonClick()" />

        <h3 class="pageTitle">Position Request (PR)</h3>

        <!-- SEARCH AND LIST POSITION REQUEST FORMS -->
        <asp:Panel ID="panList" runat="server">
            
        </asp:Panel>

        <!-- ADD POSITION REQUEST FORM -->
        <asp:Panel ID="panAdd" runat="server">
           <asp:Label ID="lblUploadError1" runat="server" style="color:Red"></asp:Label>
            <div class="title">
                ORIGINATOR:&nbsp;<span class="normal"><asp:Label ID="lblOrigName1" runat="Server"></asp:Label></span>&nbsp;ctcLink ID:&nbsp;<span class="normal"><asp:Label ID="lblOrigSID1" runat="Server"></asp:Label></span>
            </div>
           
            <div class="section"> Position ID: {{positionById.PositionID}}
                <!-- Official position title-->
                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:50%;float:left;">
                        <strong>Official position title*</strong><br />
                        <input required name="OfficialTitle" ng-model="positionById.OfficialTitle" style="width:90%"> {{positionById.OfficialTitle}}
                     </span>
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Working title</strong><br />
                        <input ng-model="positionById.WorkingTitle" style="width:80%"> {{positionById.WorkingTitle}}
                    </div>
                     <div style="width:20%;float:left;">
                        <strong>Class #*</strong><br />
                        <input ng-model="positionById.ClassNo" style="width:100%"> 
                    </div>
                    <div class="clearer"></div>
                </div>

               

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:40%;float:left;">
                        <strong>College/Unit *</strong><br />
                        <select ng-model="positionById.College">
                            <option ng-repeat="x in units" value="{{x.no}}">{{x.unitName}}</option>
                        </select>
                        
                        {{positionById.College}}
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Budget #*</strong><br />
                        <input ng-model="positionById.BudgetNo" style="width:90%"> 
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Position control </strong><br />
                        <input ng-model="positionById.PositionControlNo" style="width:100%"> 
                    </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:70%;float:left;">
                        <strong>Department Name*</strong><br /> 
                        <select ng-model="selectedDepartment"  
                                ng-options="item as item.departmentName for item in departments | filter : { unit : positionById.College}">

                        </select> 
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Department #</strong>
                        <div style="padding-top:3px">
                            <input type="text" ng-model="selectedDepartment.departmentNo" id="departmentNo" disabled><br>
                        </div>
                        <!--<div style="padding-top:3px"><asp:label ID="lblOrigDeptNum1" runat="server"><input type="text" id="departmentNo" placeholder="Auto Populate" disabled><br></asp:label></div> -->
                    </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:70%;float:left;">
                        <strong>Position phone # *</strong><br />
                        <input ng-model="positionById.PhoneNo" style="width:80%"></input> 
                        
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Mail stop*</strong><br />
                        <input ng-model="positionById.MailStop" style="width:100%"> 
                    </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:5px 0px 5px 5px; height: 5%">
                    <div style="width:70%; height:20px; float:left;">
                        <strong>Immediate Supervisor</strong><br />
                        <div class="dropdown">
                            <input type="text" name="country" id="country" ng-model="positionById.Supervisor" ng-keyup="complete(positionById.Supervisor)" class="form-control" />
                            <ul class="list-group" ng-model="hidethis" ng-hide="hidethis">
                                <li class="list-group-item" ng-repeat="employee in filterEmp" ng-click="fillTextbox(employee)">{{employee.empName}} | {{employee.deptName}}</li>
                            </ul>
                         </div>
                    </div>
                    <div style="width:30%;float:left;">
                        <strong>Supervisor's EMPID</strong><br />
                        <!-- <div style="padding-top:3px"><asp:label ID="Label1" runat="server">Auto populate</asp:label></div> -->
                        <input ng-model="positionById.SupervisorID" style="width:100%" disabled> 
                    </div><br /><br /><br />
                    <div class="clearer"></div>
                </div>

            </div> 
            
            <!-- End div seach emp -->
            <div class="title">Type of position</div>
            <div class="section">
                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:100%;float:left;">
                        <div style="width:50%;float:left;" id="newReplace">
                                <!--<div style="width:50%;float:left;"><input type="radio" id="new" name="newReplace" runat="server" />New </div>
                                <div style="width:50%;float:left;"><input type="radio" id="replace" name="newReplace" runat="server" />Replace</div> -->
                                <input type="radio" ng-model="positionById.NewReplace" value="new">New
                                <input type="radio" ng-model="positionById.NewReplace" value="replace">Replace
                            </div>  
                            <div style="width:50%;float:left;" ng-show="positionById.NewReplace == 'replace'">
                                <strong>Name of employee replaced</strong><br />
                                <input type="text" ng-model="positionById.NameReplace" style="width:95%"></p> 
                            </div>
                     </div>
                    <div class="clearer"></div>
                </div>

                <div style="padding:0px 0px 5px 5px;">
                    <div style="width:100%;float:left;">
                        <!-- <div style="width:30%; float:left; font-weight:bold;">Type of position *</div> -->
                        <div style="width:50%; float:left;">
                            <div style="font-weight:bold;">Type of position *</div>
                            <select ng-model="positionById.PositionType">
                                <option ng-repeat="x in positionTypes" value="{{x.value}}">{{x.name}}</option>
                            </select>
                        </div>
                        <div style="width:50%;float:left;"  ng-show="positionById.PositionType == 'hourly'">
                            Attach files
                         </div>
                        <div style="width:50%;float:left;"  ng-show="positionById.PositionType == 'classified'">
                            <div style="width:50%;float:left;">
                                <input type="radio" ng-model="classifiedType" value="permanent">Permanent <br />
                                <input type="radio" ng-model="classifiedType" value="nonPermanent">Non-Permanent
                            </div>
                            <div style="width:50%;float:left;" ng-show="classifiedType == 'nonPermanent'">
                                <strong>End Date</strong><br />
                                <input type="text" ng-model="endDate" style="width:95%"></p> {{endDate}}
                            </div>
                        </div>
                        <div class="clearer"></div>
                        
                    </div>
                    
                </div>
                
                

                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:100%;float:left;" id="recruitmentType">
                            <div><strong>Type of recruitment</strong></div>
                            <div style="width:50%; float:left;">
                                <select ng-model="positionById.RecruitmentType">
                                    <option ng-repeat="x in recruitmentTypes |  filter : filterRecruitment()" value="{{x.value}}">{{x.name}}</option>
                                </select>
                            </div>
                        </div>
                    <div class="clearer"></div>
                    
                </div>
                
                <div ng-show="selectedPositionType == 'classified' || selectedPositionType == 'hourly' ">
                    <strong>Position work schedule</strong>
                    <div style="padding:5px 0px 5px 5px;">
                        <div style="width:30%;float:left;">
                                <strong># of hours per day</strong><br />
                            </div>
                            <div style="width:30%;float:left;">
                                <strong># of hours per week</strong><br />
                            </div>
                            <div style="width:40%;float:left;">
                                <strong># of months per year</strong><br />
                                <input type="text" ng-model="monthYear" style="width:100%">
                                <p ng-show = "!showCalendarCode">
                                    <strong>Calendar Code</strong><br />
                                    <input type="text" ng-model="calendarCode" style="width:95%"> {{calendarCode}}
                            
                                 </p>
                            </div>
                         
                            <div class="clearer"></div>
                    </div>
                
                    <div style="padding:5px 0px 5px 5px;">
                        <div ng-repeat="x in example" style="width:100%;float:left;">  
                            <div style="width:20%;float:left;">{{x.name}}</div>
                             <div style="width:50%;float:left;">
                                    <input type="time" id="exampleInput"  ng-model="x.from" placeholder="HH:mm:ss" min="08:00:00" max="17:00:00"  />
                                    <input type="time" id="exampleInput2"  ng-model="x.to" placeholder="HH:mm:ss" min="08:00:00" max="17:00:00"  />
                                 
                            </div>

                        </div>
                        
                       
                      
                        
                        <div class="clearer"></div>
                    </div>

                </div>
                <div style="padding:5px 0px 5px 5px;">
                    <div style="width:50%;float:left;">
                        <div><strong>Onboarding</strong></div>
                        This position will require
                        <div>
                            <div style="padding-bottom:5px;padding-top:5px"><input type="checkbox" ng-model="onboard.pCard">P-Card</div> 
                            <div style="padding-bottom:5px;padding-top:5px"><input type="checkbox" ng-model="onboard.cellPhone">CCS-issued cell phone</div>
                            <div style="padding-bottom:5px;padding-top:5px"><input type="checkbox" ng-model="onboard.access">Additional ctcLink security access</div>
                        </div>
                    </div>
                    <div class="clearer"></div>
                 </div>

            </div>
            <!-- Begin Comment -->
            <div class="title"><strong>Originator Comments</strong></div>
                <div class="section" style="border-top:none;border-bottom:none">
                    <div style="padding:0px 10px 5px 5px;text-align:left">
                    </div>
                    
               
                </div>
            <!-- End Comment -->
            <!-- Begin Add File Attachments -->
             


            <div class="title"> Add File Attachments </div>
            
            <div class="section">
                <div style="padding:0px 5px 5px 5px;text-align:left">
                    <p style="margin-top:3px;padding-top:3px" id="uploadArea" >
                        <label for="myFileField">Select a file: </label>
                        <input type="file" demo-file-model="myFile"  class="form-control" id ="myFileField"/>
                        <button ng-click="uploadFile()" class = "btn btn-primary">Upload File</button>
                        <button ng-click = "removeFile()">Remove File</button>
                    </p>
                    <div style="width:100%;text-align:right">
                        <button ng-click = "addAnotherFile()">Add another file</button>
                        {{fileArr}} 
                        {{fileCount}}
                    </div>
                </div>
            </div>
            
            <!-- End Add File Attachments -->
            <div style="width:100%;text-align:center;padding-top:20px">
                <input type="button" value="Back" id="cmdBack1" style="width:70px" onclick="history.back(1)" />&nbsp;
                <button ng-click="saveEmplist()" >SaveEmplist</button>
                <button  ng-click="save()" >Save</button>
                <button  ng-click="view()" >View</button>
                <input type="text" ng-model="positionId" style="width:100%">
                <button ng-click="getPositionById()" >Get Position by {{positionId}}</button>
            </div>
           
        </asp:Panel>

        <!-- EDIT POSITION REQUEST FORM -->
        <asp:Panel ID="panEdit" runat="server">
            
        </asp:Panel>

        <!-- VIEW POSITION REQUEST FORM -->
        <asp:Panel ID="panView" runat="server">
            <!-- view originator PR form fields go here -->
        </asp:Panel>

        <!-- CONFIRM SAVED / SUBMITTED POSITION REQUEST FORM -->
        <asp:Panel ID="panConfirm" runat="server">

        </asp:Panel>

        <!-- DISPLAY ERROR MESSAGE -->
        <asp:Panel ID="panError" runat="server"></asp:Panel>
     </form> 
</div>
</asp:Content>

