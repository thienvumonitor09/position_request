﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="Form.aspx.cs" Inherits="Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
     
    <style>
         .autocomplete {
          /*the container must be positioned relative:*/
          width : 100%;
          position: relative;
          display: inline-block;
        }
         .autocomplete:hover {
                position: relative;
                display: inline-block;
           }
         
         .autocomplete-items{
           position: absolute;
          border-bottom: none;
          border-top: none;
          z-index: 99;
          /*position the autocomplete items to be the same width as the container:*/
          top: 100%;
          left: 0;
          right: 0;
        }

         .autocomplete-items div {
             width : 150%;
          padding:10px;
          cursor: pointer;
          background-color: #fff; 
          border-bottom: 1px solid #d4d4d4; 
        }
         .autocomplete-items div:hover {
            /*when hovering an item:*/
            background-color: #e9e9e9; 
          }
          
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" Runat="Server">
    <div ng-app="myApp" ng-controller="myCtrl">
    <form id="frmPR" runat="server" enctype="multipart/form-data">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
        <input type="hidden" id="hidDepartment" runat="server" />
        <input type="hidden" id="hidDepartmentID" runat="server" />

        <h3 class="pageTitle">Position Request (PR)</h3>

        <!-- SEARCH AND LIST POSITION REQUEST FORMS -->
        <asp:Panel ID="panList" runat="server">
            
        </asp:Panel>

        <!-- ADD POSITION REQUEST FORM -->
        <asp:Panel ID="panAdd" runat="server">
            <asp:Label ID="lblUploadError" runat="server" style="color:Red"></asp:Label>
            <div class="title">
                ORIGINATOR:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorName" runat="Server">Vaughn,Brandy R</asp:Label></span>
                &nbsp;ctcLink ID:&nbsp;<span class="normal">101000450<asp:Label ID="lblOriginatorID" runat="Server"></asp:Label></span>
            </div>
            <div class="section" style="border-bottom: none;">
                <div class="row">
                    <div class="col" style="width: 264px;">
                        <strong>Official Position Title</strong><br />
                        <asp:TextBox ID="txtOfficialTitle" runat="Server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                    </div>
                    <div class="col" style="width: 264px;">
                        <strong>Working Title</strong><br />
                        <asp:TextBox ID="txtWorkingTitle" runat="Server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 31%;">
                        <strong>Job Code</strong><br />
                        <asp:TextBox ID="txtJobCode" runat="Server" MaxLength="6" Style="width: 100%"></asp:TextBox>
                    </div>
                    <div class="col" style="width: 31%;">
                        <strong>Budget #</strong><br />
                        <asp:TextBox ID="txtBudgetNumber" runat="Server" MaxLength="75" Style="width: 100%"></asp:TextBox>
                    </div>
                    <div class="col" style="width: 31%;">
                        <strong>Position Control #</strong><br />
                        <asp:TextBox ID="txtPositionNumber" runat="Server" MaxLength="8" Style="width: 100%"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 31%">
                        <strong>College/Unit</strong><br />
                        <asp:DropDownList ID="cboCollegeUnit" runat="server" Style="width: 100%">
                            <asp:ListItem Value="">-- Select One --</asp:ListItem>
                            <asp:ListItem Value="District">District</asp:ListItem>
                            <asp:ListItem Value="SCC">SCC</asp:ListItem>
                            <asp:ListItem Value="SFCC">SFCC</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col" style="width: 250px;">
                        <strong>Department Name</strong><br />
                        <asp:DropDownList ID="cboDepartment" runat="Server" Style="width: 100%;">
                            <asp:ListItem Value="">-- Select One --</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col">
                        <strong>Department #</strong><br />
                        <asp:Label ID="lblDepartmentID" runat="server">Auto Populated</asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Position Phone #</strong><br />
                        <asp:TextBox ID="txtPhoneNumber" runat="Server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>Mail Stop</strong><br />
                        <asp:TextBox ID="txtMailStop" runat="server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Immediate Supervisor</strong><br />
                        <!--<asp:TextBox ID="txtSupervisorName" runat="Server" MaxLength="30" Style="width: 100%"></asp:TextBox> -->
                        <!--
                        <div class="autocomplete">
                            <input type="text" name="country" id="country" ng-model="positionById.Supervisor" ng-keyup="complete(positionById.Supervisor)" style="width: 100%;" />             
                                <div id="myInputautocomplete-list" class="autocomplete-items" ng-model="hidethis" ng-hide="hidethis">
                                    <div ng-repeat="employee in filterEmp" ng-click="fillTextbox(employee)" ng-hide="hidethis">
                                        {{employee.empName}} | {{employee.deptName}}
                                    </div>
                                 </div> -->
                                <!-- <li class="list-group-item" ng-repeat="employee in filterEmp" ng-click="fillTextbox(employee)">{{employee.empName}} | {{employee.deptName}}</li>-->
                           <!-- 
                         </div> -->
                       <!-- <asp:Label ID="lblSupervisorNam2e" runat="Server" style="width: 100%">Bob Nelson</asp:Label> -->
                        <asp:TextBox ID="lblSupervisorName" runat="Server" Style="width: 100%"></asp:TextBox>
                        <!--<input id="lblSupervisorName"> -->
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>Supervisor's ctcLink ID</strong><br />
                        <asp:Label ID="lblSupervisorID" runat="server"></asp:Label>
                    </div>
                </div>
                <!-- Show supervisor list here -->
            </div>
            <div style="display: flex; overflow: hidden">
                <div style="width: 50%;">
                    <div class="title" style="width: 100%">TYPE OF POSITION</div>
                    <div class="section" style="width: 100%; height: 100%;">
                        <div class="row">
                            <div id="newReplacement" class="col" style="margin-left: -5px; padding-bottom: 5px;">
                                <input type="radio" id="optNew" name="optNewReplacement" runat="server" /><strong>New</strong> &nbsp;
                                <input type="radio" id="optReplacement" name="optNewReplacement" runat="server" /><strong>Replacement</strong>
                            </div>
                            <div id="employeeReplaced" style="margin-left: 15px; padding-top: 10px;">
                                Name of Employee Replaced<br />
                                <asp:TextBox ID="txtEmployeeReplaced" runat="server" MaxLength="30" Style="width: 70%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;">
                                <div class="col" style="text-align: left;">
                                    <strong>Type of Position</strong>
                                    <asp:DropDownList ID="cboPositionType" runat="server" Style="width: 93%;">
                                        <asp:ListItem Value="">-- Select One --</asp:ListItem>
                                        <asp:ListItem Value="Classified">Classified</asp:ListItem>
                                        <asp:ListItem Value="Part-Time_Hourly">Part-Time Hourly</asp:ListItem>
                                        <asp:ListItem Value="Tenure-Track_Faculty">Tenure-Track Faculty</asp:ListItem>
                                        <asp:ListItem Value="Non_Tenure-Track_Faculty">Non Tenure-Track Faculty</asp:ListItem>
                                        <asp:ListItem Value="Adjunct_Faculty">Adjunct Faculty</asp:ListItem>
                                        <asp:ListItem Value="Professional/Confidential_Exempt">Professional/Confidential Exempt</asp:ListItem>
                                        <asp:ListItem Value="Administrator/Executives">Administrator/Executives</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col" id="worksheet">
                                    Link for worksheet
                                </div>
                                <div class="col" id="classified">
                                    <div class="col" style="margin-left: 10px; padding-top: 15px; padding-bottom: 10px;">
                                        <input type="radio" id="optPermanent" name="optClassified" runat="server" />Permanent &nbsp;
                                        <input type="radio" id="optNonPermanent" name="optClassified" runat="server" />Non-Permanent
                                    </div>
                                    <div class="col" id="endDate" style="margin-left: 124px; padding-bottom: 10px;">
                                        End Date<br />
                                        <input type="text" id="txtEndDate" name="txtEndDate" class="datepicker" runat="server" maxlength="10" style="width: 90%" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;" id="recruitmentType">
                                <strong>Type of Recruitment</strong>
                                <div>
                                    <asp:DropDownList ID="cboRecruitmentType" runat="server" style="width: 90%">
                                        <asp:ListItem Value="" Text="">-- Select One --</asp:ListItem>
                                        <asp:ListItem Value="Promotional" Text="Promotional"></asp:ListItem>
                                        <asp:ListItem Value="Internal" Text="Internal"></asp:ListItem>
                                        <asp:ListItem Value="Open_Competive" Text="Open Competive"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-right: 5px; width: 48%">
                    <div class="title" style="width: 100%; padding-right: 5px;">ONBOARDING</div>
                    <div class="section" style="width: 100%; padding-right: 0; padding-bottom:15px; height: 100%;">
                        <p><strong>This position will require:</strong></p>
                        <div style="padding-top: 5px; margin-left: 10px;">
                            <asp:CheckBox ID="pCard" runat="server" />P-Card
                        </div>
                        <div style="padding-top: 5px; margin-left: 10px;">
                            <asp:CheckBox ID="cellPhone" runat="server" />CCS-issued Cell Phone
                        </div>
                        <div style="padding-top: 5px; margin-left: 10px;">
                            <asp:CheckBox ID="addAccess" runat="server" />Additional ctcLink Security Access
                        </div>
                        <div style="padding-top: 5px; margin-left: 10px;">
                            <asp:CheckBox ID="manager" runat="server" />Manager/Timesheet Approver
                        </div>
                    </div>
                </div>
            </div>
            <div id="schedule">
                <div class="title">POSITION WORK SCHEDULE</div>
                <div class="section" style="border-bottom: none;">
                    <div class="row">
                        <div class="col">
                            <strong>Hours Per Day</strong><br />
                            <asp:TextBox ID="txtHoursPerDay" runat="server" MaxLength="30" Style="width: 115px"></asp:TextBox>
                        </div>
                        <div class="col">
                            <strong>Hours Per Week</strong><br />
                            <asp:TextBox ID="txtHoursPerWeek" runat="server" MaxLength="30" Style="width: 115px"></asp:TextBox>
                        </div>
                        <div class="col">
                            <strong>Months Per Year</strong><br />
                            <asp:TextBox ID="txtMonthsPerYear" runat="server" MaxLength="30" Style="width: 115px"></asp:TextBox>
                        </div>
                        <div id="cyclicCalendarCode" class="col">
                            <strong>Cyclic Calendar Code</strong><br />
                            <asp:TextBox ID="txtCyclicCalendarCode" runat="server" MaxLength="30" Style="width: 140px"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row" style="white-space:nowrap;">
                        <div class="timeCol">
                            <strong>Monday</strong><br />
                            <input type="text" id="txtMondayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                            <input type="text" id="txtMondayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                        </div>
                        <div class="timeCol">
                            <strong>Tuesday</strong><br />
                            <input type="text" id="txtTuesdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                            <input type="text" id="txtTuesdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                        </div>
                        <div class="timeCol">
                            <strong>Wednesday</strong><br />
                            <input type="text" id="txtWednesdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                            <input type="text" id="txtWednesdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                        </div>
                        <div class="timeCol">
                            <strong>Thursday</strong><br />
                            <input type="text" id="txtThursdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                            <input type="text" id="txtThursdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                        </div>
                        <div class="timeCol">
                            <strong>Friday</strong><br />
                            <input type="text" id="txtFridayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                            <input type="text" id="txtFridayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                        </div>
                        <div class="timeCol">
                            <strong>Saturday</strong><br />
                            <input type="text" id="txtSaturdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                            <input type="text" id="txtSaturdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                        </div>
                        <div class="timeCol">
                            <strong>Sunday</strong><br />
                            <input type="text" id="txtSundayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                            <input type="text" id="txtSundayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                        </div>
                    </div>
                    <div style="width: 100%; text-align: center; padding-top: 15px;padding-bottom:15px;">
                        <button id="btnApplyAll" type="button">Apply All</button>
                        <button id="btnReset" type="button">Reset</button>
                    </div>
                </div>
            </div>
            <div class="title">COMMENTS</div>
            <div class="section" style="border-top: none; border-bottom: none;">
                <div style="padding: 0px 10px 5px 5px; text-align: left">
                    <asp:TextBox ID="txtComments" runat="server" Style="width: 100%" Rows="4" TextMode="multiLine" MaxLength="4000"></asp:TextBox>
                </div>
            </div>
            <div class="title">FILE ATTACHMENTS</div>
            <div class="section">
                <div style="padding: 0px 5px 5px 5px; text-align: left">
                    <p style="margin-top: 3px; padding-top: 3px" id="uploadArea" runat="server">
                        <input id="file" style="font-size: 9pt" type="file" runat="server" size="72" />
                    </p>
                    <div style="width: 100%; text-align: right">
                        <input id="btnAddFile" type="button" value="Add Another File" onclick="fileUploadBox()" />
                    </div>
                </div>
            </div>
            <div style="width: 100%; text-align: center; padding-top: 20px">
                <input type="button" value="Back" id="btnBack" style="width: 70px" onclick="history.back(1)" />&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" Style="width: 70px" />&nbsp;
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Style="width: 70px" />
            </div>
        </asp:Panel>

        <!-- EDIT POSITION REQUEST FORM -->
        <asp:Panel ID="panEdit" runat="server">
            
        </asp:Panel>

        <!-- VIEW POSITION REQUEST FORM -->
        <asp:Panel ID="panView" runat="server">
            <!-- view originator PR form fields go here -->
        </asp:Panel>

        <!-- CONFIRM SAVED / SUBMITTED POSITION REQUEST FORM -->
        <asp:Panel ID="panConfirm" runat="server">

        </asp:Panel>

        <!-- DISPLAY ERROR MESSAGE -->
        <asp:Panel ID="panError" runat="server"></asp:Panel>
    </form>
    </div>
    <script type="text/javascript">
        $(function () {
            getEmployees();
            //var empList = fillEmplist();
            //console.log(empList);
            var availableTags = [
                "ActionScript",
                "AppleScript",
                "Asp",
                "BASIC",
                "C",
                "C++",
                "Clojure",
                "COBOL",
                "ColdFusion",
                "Erlang",
                "Fortran",
                "Groovy",
                "Haskell",
                "Java",
                "JavaScript",
                "Lisp",
                "Perl",
                "PHP",
                "Python",
                "Ruby",
                "Scala",
                "Scheme",
                "Vu Nguyen",
                "Bob Nelson"
            ];

            var empList = [
                { "empId": "101002545", "empName": "Aamold,Marianne C", "deptName": "SCC ESL" },
                { "empId": "101002129", "empName": "Abbott,Rindi L", "deptName": "HS Northeast CC" },
                { "empId": "101009452", "empName": "Abdallah,Damon M", "deptName": "SFCC Music \u0026 Drama" },
                { "empId": "201099300", "empName": "Abe-Gunter,Morgan R", "deptName": "SCC Pace Services" },
                { "empId": "101003512", "empName": "Ableman,Shantel F", "deptName": "College Bookstores" },
                { "empId": "101002256", "empName": "Ables,Tyler B", "deptName": "District Security" },
                { "empId": "201276938", "empName": "Abraha,Merhawi Asgele", "deptName": "SFCC Mathematics" },
                { "empId": "101009532", "empName": "Abraham,Jeremy R", "deptName": "SCC Off Campus Newport" },
                { "empId": "101009320", "empName": "Abrahamson,Kimberly J", "deptName": "SFCC Facilities" },
                { "empId": "201247605", "empName": "Abrams,Shirley", "deptName": "HS SFCC ELC" }
            ];

            $("#<%= lblSupervisorID.ClientID %>").html("Auto Populated");

            var demoArr = [];
            $.each(empList, function (index, value) {
                //console.log(value.empId);
                demoArr.push({
                    label: value.empName + " | " + value.deptName,
                    value: value.empName,
                    des: value.empId
                });
            });

            var list2 = [
                { "label": "India", "value": "IND" },
                { "label": "Australia", "value": "AUS" }
            ];

            $("#<%= lblSupervisorName.ClientID %>").autocomplete({
                source: demoArr,
                select: function (event, ui) {
                    //alert(ui.item.value);
                    $("#<%= lblSupervisorID.ClientID %>").html(ui.item.des);
                }
            });



            $("#classified").hide();
            $("#endDate").hide();
            $("#employeeReplaced").hide();
            $("#schedule").hide();
            $("#cyclicCalendarCode").hide();
            $("#worksheet").hide();

            $("#<%= cboCollegeUnit.ClientID %>").change(getDepartments);

            $("#<%=cboPositionType.ClientID%>").change(function () {

                if ($("#<%=cboPositionType.ClientID%>").val() == "Classified") {
                    $("#classified").show();
                } else {
                    $("#classified").hide();
                }

                if ($("#<%=cboPositionType.ClientID%>").val() == "Hourly") {
                    $("#worksheet").show();
                } else {
                    $("#worksheet").hide();
                }

                //check type of recruitment
                $("#<%=cboRecruitmentType.ClientID%> option[value='No_Recruitment']").remove();
                if ($("#<%=cboPositionType.ClientID%>").val() == "Hourly" || $("#<%=cboPositionType.ClientID%>").val() == "Adjunct_Faculty") {
                    $("#<%=cboRecruitmentType.ClientID%>").append("<option value='No_Recruitment'>No Recruitment</option>");
                }
            });

            $("#classified").change(function () {
                $("#<%=cboRecruitmentType.ClientID%> option[value='No_Recruitment']").remove();
                if ($("#<%=optNonPermanent.ClientID%>").is(":checked")) {
                    $("#endDate").show();
                    $("#<%=txtEndDate.ClientID%>").focus();

                    //add non-recruitment to type of recruitment
                    $("#<%=cboRecruitmentType.ClientID%>").append("<option value='No_Recruitment'>No Recruitment</option>");
                } else {
                    $("#endDate").hide();
                }
            });

            $("#newReplacement").change(function () {
                if ($("#<%=optReplacement.ClientID%>").is(":checked")) {
                    $("#employeeReplaced").show();
                    $("#<%=txtEmployeeReplaced.ClientID%>").focus();
                } else {
                    $("#employeeReplaced").hide();
                }
            });

            $("#<%=cboPositionType.ClientID%>").change(function () {
                if ($("#<%=cboPositionType.ClientID%>").val() == "Classified" || $("#<%=cboPositionType.ClientID%>").val() == "Hourly") {
                    $("#schedule").show();
                } else {
                    $("#schedule").hide();
                }
            });

            $("#<%=txtMonthsPerYear.ClientID%>").change(function () {
                if (parseInt($("#<%=txtMonthsPerYear.ClientID%>").val()) < 12) {
                    $("#cyclicCalendarCode").show();
                    $("#<%=txtCyclicCalendarCode.ClientID%>").focus();
                } else {
                    $("#cyclicCalendarCode").hide();
                }
            });

            $("#btnApplyAll").click(function () {
                var mondayStart = $("#<%= txtMondayStart.ClientID %>").val();
                var mondayEnd = $("#<%= txtMondayEnd.ClientID %>").val();
                $("#<%= txtTuesdayStart.ClientID %>").val(mondayStart);
                $("#<%= txtWednesdayStart.ClientID %>").val(mondayStart);
                $("#<%= txtThursdayStart.ClientID %>").val(mondayStart);
                $("#<%= txtFridayStart.ClientID %>").val(mondayStart);
                $("#<%= txtSaturdayStart.ClientID %>").val(mondayStart);
                $("#<%= txtSundayStart.ClientID %>").val(mondayStart);

                $("#<%= txtTuesdayEnd.ClientID %>").val(mondayEnd);
                $("#<%= txtWednesdayEnd.ClientID %>").val(mondayEnd);
                $("#<%= txtThursdayEnd.ClientID %>").val(mondayEnd);
                $("#<%= txtFridayEnd.ClientID %>").val(mondayEnd);
                $("#<%= txtSaturdayEnd.ClientID %>").val(mondayEnd);
                $("#<%= txtSundayEnd.ClientID %>").val(mondayEnd);
            });

            $("#btnReset").click(function () {
                var mondayStart = $("#<%= txtMondayStart.ClientID %>").val();
                var mondayEnd = $("#<%= txtMondayEnd.ClientID %>").val();
                $("#<%=txtMondayStart.ClientID%>").val("");
                $("#<%=txtTuesdayStart.ClientID%>").val("");
                $("#<%=txtWednesdayStart.ClientID%>").val("");
                $("#<%=txtThursdayStart.ClientID%>").val("");
                $("#<%=txtFridayStart.ClientID%>").val("");
                $("#<%=txtSaturdayStart.ClientID%>").val("");
                $("#<%=txtSundayStart.ClientID%>").val("");

                $("#<%= txtMondayEnd.ClientID %>").val("");
                $("#<%= txtTuesdayEnd.ClientID %>").val("");
                $("#<%= txtWednesdayEnd.ClientID %>").val("");
                $("#<%= txtThursdayEnd.ClientID %>").val("");
                $("#<%= txtFridayEnd.ClientID %>").val("");
                $("#<%= txtSaturdayEnd.ClientID %>").val("");
                $("#<%= txtSundayEnd.ClientID %>").val("");
            });

        });

        function getDepartments() {
            $.ajax({
                type: "POST",
                url: "Form.aspx/PopulateDepartments",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert(JSON.stringify(response.d));
                    //remove all options from the department dropdownlist
                    $("#<%= cboDepartment.ClientID %>").removeOption(/./);

                    //if a college unit is not selected
                    if ($("#<%= cboCollegeUnit.ClientID %>").val() == "") {
                        $("#<%= cboDepartment.ClientID %>").addOption("", "-- Select One --");
                        $("#<%= lblDepartmentID.ClientID %>").html("Auto Populated");
                    } else {
                        //populate the department dropdownlist
                        $.each(response.d, function () {
                            $("#<%= cboDepartment.ClientID %>").addOption(this["Value"], this["Text"], false);
                        });
                    }

                    //onchange of the department dropdownlist populate the department number label
                    $("#<%= cboDepartment.ClientID %>").change(function () {
                        $("#<%= lblDepartmentID.ClientID %>").html($(this).val() == "" ? "Auto Populated" : $(this).val());
                        $("#<%= hidDepartmentID.ClientID %>").val($(this).val());
                        $("#<%= hidDepartment.ClientID %>").val($("#<%= cboDepartment.ClientID %> option:selected").text());
                    });
                }
            });
        }

        function getEmployees() {

            $.ajax({
                type: "POST",
                url: 'WebService.asmx/GetEmpList',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    alert(JSON.stringify(response.d));
                    //empList = response.d;
                    //alert(empList);
                    //fillEmplist(response.d);
                    //return response.d;
                },
                error: function (xhr, textStatus, error) {
                    alert(error);
                }
            });
        }

        function fillEmplist(data) {
            console.log(data);
            return data;
            //empList = data;
        }

        function fileUploadBox() {
            if (!document.getElementById || !document.createElement)
                return false;

            var uploadArea = document.getElementById("<%= uploadArea.ClientID %>");

            if (!uploadArea)
                return;

            var newLine = document.createElement("div");
            newLine.style.padding = "3px";
            uploadArea.appendChild(newLine);

            var newUploadBox = document.createElement("input");

            // Set up the new input for file uploads
            newUploadBox.type = "file";
            newUploadBox.size = "72";
            newUploadBox.style.fontSize = "9pt";

            // The new box needs a name and an ID
            if (!fileUploadBox.lastAssignedId)
                fileUploadBox.lastAssignedId = 100;

            newUploadBox.setAttribute("id", "dynamic" + fileUploadBox.lastAssignedId);
            newUploadBox.setAttribute("name", "dynamic:" + fileUploadBox.lastAssignedId);
            uploadArea.appendChild(newUploadBox);
            fileUploadBox.lastAssignedId++;
        }

    </script>
</asp:Content>

