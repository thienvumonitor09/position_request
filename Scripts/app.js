﻿
var app = angular.module('myApp', []);
/*
    A directive to enable two way binding of file field
    */
app.directive('demoFileModel', function ($parse) {
    return {
        restrict: 'A', //the directive can be used as an attribute only

        /*
         link is a function that defines functionality of directive
         scope: scope associated with the element
         element: element on which this directive used
         attrs: key value pair of element attributes
         */
        link: function (scope, element, attrs) {
            var model = $parse(attrs.demoFileModel),
                modelSetter = model.assign; //define a setter for demoFileModel

            //Bind change event on the element
            element.bind('change', function () {
                //Call apply on scope, it checks for value changes and reflect them on UI
                scope.$apply(function () {
                    //set the model value
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
});

app.service('fileUploadService', function ($http, $q) {

    this.uploadFileToUrl = function (file, uploadUrl) {
        //FormData, object of key/value pair for form fields and values
        var fileFormData = new FormData();
        fileFormData.append('file', file);

        var deffered = $q.defer();
        $http.post(uploadUrl, fileFormData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (response) {
            deffered.resolve(response);
            }, function (error) {
                deffered.reject(response);
            });
        return deffered.promise;
    }
});



/*
app.filter('filterRecruitment', function () {
    return function (item) {
        if (item.value == "non_recruitment")
            return false;
        else
            return true;
    };
});*/
app.controller('myCtrl', function ($scope, $http, fileUploadService) {

    $scope.positionById = {};
    
    $scope.positionById = {
        "OfficialTitle": "o", "WorkingTitle": "w", "ClassNo": "c", "College": "170", "BudgetNo": "b", "PositionControlNo": "p",
        "Department": "Administration", "DepartmentNo": "99000",
        "PhoneNo": "123", "MailStop": "456", "Supervisor": "Abad,Dylan L M", "SupervisorID": "201350341",
        "PositionType": "classified", "RecruitmentType": "promotional", "NewReplace": "new"
    };
    $scope.updateDepartment = function () {
        $scope.positionById.Department = $scope.selectedDepartment.departmentName;
        $scope.positionById.DepartmentNo = $scope.selectedDepartment.departmentNo;
    }
    $scope.$watch('selectedDepartment', function () {
        $scope.positionById.Department = $scope.selectedDepartment.departmentName;
        $scope.positionById.DepartmentNo = $scope.selectedDepartment.departmentNo;
    });
    
    /* Get Position by Id*/
    $scope.view = function () {
        //$scope.selectedDepartment = [];
        alert(JSON.stringify($scope.onboard));
        //alert(JSON.stringify($scope.selectedDepartment));
        //alert(JSON.stringify($scope.positionById));
        //alert($scope.positionById.DepartmentNo);
        /*
        $http({
            method: "POST",
            url: "WebService.asmx/SavePositionAll",
            dataType: 'json',
            data: { positionById: JSON.stringify($scope.positionById) },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            alert(JSON.stringify(response.data));
            $scope.positionById = JSON.parse(response.data.d);
            //alert($scope.positionById.NewReplace);
            });
        */
        
        /*
        $http({
            method: "POST",
            url: "WebService.asmx/SaveNewReplace",
            dataType: 'json',
            data: { newReplace: JSON.stringify($scope.newReplace) },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            alert(JSON.stringify(response.data.d));
            //$scope.positionById = JSON.parse(response.data.d);
            //alert(response.data);
        });*/

    }
   
    $scope.getPositionById = function () {
        alert("get position by id");
        $http({
            method: 'POST',
            url: 'WebService.asmx/GetPositionAllById',
            dataType: 'json',
            data: { id: $scope.positionId },
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (response) {
            alert(JSON.stringify(response.data.d)); //.d because json has format {d:{}} ; stringify to print
            //alert(response.data);
            //console.log(JSON.stringify(response));
            $scope.positionById = JSON.parse(response.data.d); //Parse the data with JSON.parse(), and the data becomes a JavaScript object.
            alert($scope.positionById.PositionID); 
        }, function (error) {
            alert("error");
        });
    };
    

    $scope.fileArr = ["1"]; 
    $scope.fileCount = 1;

    $scope.addAnotherFile = function () {
        $scope.fileCount++;
        $scope.fileArr.push($scope.fileCount + "");

    };

    $scope.uploadFile = function () {
        var file = $scope.myFile;
        console.log('file is ');
        console.dir(file);
        var uploadUrl = "/Webservice.asmx/UploadFile"; //Url of webservice/api/server
        
        var fileFormData = new FormData();
        fileFormData.append('file', file);
        /*
            $http({
                method: "POST",
                url: "WebService.asmx/UploadFile",
                //dataType: 'json',
                data: fileFormData,
                headers: { "Content-Type": undefined },
                transformRequest: angular.identity
            }).then(function (response) {
                alert(JSON.stringify(response.data));
                //alert(response.data);
            });
        
         */
        promise = fileUploadService.uploadFileToUrl(file, uploadUrl);
        promise.then(function (response) {
            //$scope.serverResponse = response;
            $scope.serverResponse = JSON.stringify(response.data);
            //alert(response.data);
        }, function () {
            $scope.serverResponse = 'An error has occurred';
        });
        
    };

    //$scope.Name = "hello";
    $scope.Name2 = "name2";
    $scope.ButtonClick = function () {
         $http({
            method: "POST",
            url: "WebService.asmx/GetCurrentTime",
            dataType: 'json',
            data: { name: $scope.Name, name2 : $scope.Name2 },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            alert(JSON.stringify(response.data));
            //alert(response.data);
        });

        
    }
   
   
    $scope.saveEmplist = function () {
        alert("saving");
        $http({
            method: "POST",
            url: "WebService.asmx/SaveEmpList",
            dataType: 'json',
            data: { name: $scope.Name},
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            alert(JSON.stringify(response.data));
            //alert(response.data);
        });

    };

    $scope.position = {
                        "officialTitle": "",
                        "workingTitle": ""
    };
    //$scope.position.officialTitle = "Engineer";
    //$scope.position.workingTitle = "Dev2";
    //alert(JSON.stringify($scope.positionById));
    //$scope.positionById.PositionID = 21;
    $scope.save = function () {
        alert("saving");
        alert("PositionID" + $scope.positionById.PositionID);
        $http({
            method: "POST",
            url: "WebService.asmx/SavePosition",
            dataType: 'json',
            data: { positionById: JSON.stringify($scope.positionById) },
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            alert(JSON.stringify(response.data));
            $scope.positionById = JSON.parse(response.data.d);
            //alert(response.data);
        });

        alert("Your record is saved");

        if ($scope.positionById.PositionID) {
             //alert("null");
            //If existing PositionID, update exisiting record


        } else {
            //If not existing position, add new record
            
        } 

    };

    $scope.showCalendarCode = true;
 
    /*
    new Date();
new Date(value);
new Date(dateString);
new Date(year, monthIndex [, day [, hours [, minutes [, seconds [, milliseconds]]]]]);
    Where Date is called as a constructor with more than one argument, if values are greater than their logical range (e.g. 13 is provided as the month value or 70 for the minute value), the adjacent value will be adjusted. E.g. new Date(2013, 13, 1) is equivalent to new Date(2014, 1, 1), both create a date for 2014-02-01 (note that the month is 0-based). Similarly for other values: new Date(2013, 2, 1, 0, 70) is equivalent to new Date(2013, 2, 1, 1, 10) which both create a date for 2013-03-01T01:10:00.*/

    $scope.example = [
        { name: "Monday", from: new Date(1970, 0, 1, 08, 05, 0), to: new Date(1970, 0, 1, 18, 05, 0) },
        { name: "Tuesday", from: new Date(1970, 0, 1, 08, 05, 0), to: new Date(1970, 0, 1, 18, 05, 0) },
        { name: "Wednesday", from: new Date(1970, 0, 1, 08, 05, 0), to: new Date(1970, 0, 1, 18, 05, 0) },
        { name: "Thursday", from: new Date(1970, 0, 1, 08, 05, 0), to: new Date(1970, 0, 1, 18, 05, 0) },
        { name: "Friday", from: new Date(1970, 0, 1, 08, 05, 0), to: new Date(1970, 0, 1, 18, 05, 0) },
        { name: "Saturday", from: new Date(1970, 0, 1, 08, 05, 0), to: new Date(1970, 0, 1, 18, 05, 0) },
        { name: "Sunday", from: new Date(1970, 0, 1, 08, 05, 0), to: new Date(1970, 0, 1, 18, 05, 0) }
    ];
    $scope.filterRecruitment = function () {
        return function (item) {
            if (item.value == 'non_recruitment') {
                if (($scope.positionById.PositionType == 'classified' && $scope.classifiedType == 'nonPermanent')
                    || $scope.positionById.PositionType == 'hourly'
                    || $scope.positionById.PositionType == 'adjunct')
                    return true;
                else
                    return false;
            }
            return true;
        };
        
        //if ($scope.selectedPositionType == 'classified' && $scope.classifiedType == 'nonPermanent')
          //  return true;
        return false;
        //return "non_recruitment";
    }
    
    $scope.units = ["District", "SCC", "SFCC"];
//$scope.departments = ["ISDS", "Marketing", "HR"];
$scope.positions = ["Classified", "Part-time hourly", "Tenure-track faculty", "Non tenure-track faculty", "Adjunct faculty",
            "Professional/confidential exempt", "Administrator/executives"];
$scope.recruitmentTypes = [
    { value: "promotional", name: "Promotional" },
    { value: "internal", name: "Internal" },
    { value: "open_competive", name: "Open competive" },
    { value: "non_recruitment", name: "Non Recruitment" }
];
        $scope.units = [
            { no: "170", unitName: "District" },
            { no: "171", unitName: "SCC" },
            { no: "172", unitName: "SFCC" }
        ];

        $scope.positionTypes = [
            { value: "classified", name: "Classified" },
            { value: "hourly", name: "Part-time hourly" },
            { value: "tenure", name: "Tenure-track faculty" },
            { value: "non_tenure", name: "Non tenure-track faculty" },
            { value: "adjunct", name: "Adjunct faculty" },
            { value: "professional", name: "Professional/confidential exempt" },
            { value: "admin", name: "Administrator/executives" }
        ];

        
        $scope.populateSchedule = function () {
            $scope.from2 = $scope.from1;
            $scope.from3 = $scope.from1;
            $scope.from4 = $scope.from1;
            $scope.from5 = $scope.from1;
            $scope.from6 = $scope.from1;
            $scope.from7 = $scope.from1;

            $scope.to2 = $scope.to1;
            $scope.to3 = $scope.to1;
            $scope.to4 = $scope.to1;
            $scope.to5 = $scope.to1;
            $scope.to6 = $scope.to1;
            $scope.to7 = $scope.to1;
        };

        $scope.resetSchedule = function () {
    $scope.from1 = "";
$scope.from2 = "";
            $scope.from3 = "";
            $scope.from4 = "";
            $scope.from5 = "";
            $scope.from6 = "";
            $scope.from7 = "";
            $scope.to1 = "";
            $scope.to2 = "";
            $scope.to3 = "";
            $scope.to4 = "";
            $scope.to5 = "";
            $scope.to6 = "";
            $scope.to7 = "";
        }

    /* Begin Auto complete Immediate supervisor name */
    $scope.employeeList = []; 
    $scope.complete = function (string) {
        $scope.hidethis = false;
        var output = [];
        angular.forEach($scope.employeeList, function (value, key) {
            if (value.empName.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
            output.push(value);
            }
        });

        $scope.filterEmp = output;
        if (string == "") {
            $scope.hidethis = true;
        }
    }
    $scope.fillTextbox = function (string) {
        //$scope.searchName = string.empName;
        //$scope.empId = string.empId;
        $scope.positionById.Supervisor = string.empName;
        $scope.positionById.SupervisorID = string.empId;
        $scope.hidethis = true;
        
        console.log($scope.positionById.Supervisor + " " + $scope.positionById.SupervisorID);
    }
    
    //get Employee List
    $scope.fillList = function () {
        $http({
            method: 'POST',
            url: 'WebService.asmx/GetEmpList',
            dataType: 'json',
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function (response) {
            //alert(JSON.stringify(response.data));
            //console.log(JSON.stringify(response));
            $scope.employeeList = response.data;
        }, function (error) {
            alert("error");
        });
    };

    $scope.fillList();
    /* End Auto complete Immediate supervisor name */
    
    //$scope.here = "here";
  
        $scope.departments = [];
        $scope.fillDepartmentList = function () {
    $http({
        method: 'POST',
        url: 'WebService.asmx/GetDepartmentList',
        dataType: 'json',
        headers: {
            "Content-Type": "application/json"
        }
    }).then(function (response) {
        $scope.departments = response.data;
    }, function (error) {
        alert("error");
    });

};
        $scope.fillDepartmentList();

    });
   