﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form3 : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {


        //string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=PR;Integrated Security=true;";
        string strConnection = "Data Source=localhost;Workstation ID=internalEAN;Initial Catalog=PR;Integrated Security=true;";
        SqlConnection myConnection = new SqlConnection(strConnection);
        myConnection.Open();
        string query = "SELECT TOP (10) [EMPLID],[Name] FROM [ctcLink_ODS].[dbo].[Names_HC]";
        SqlCommand selectCommand = new SqlCommand(query, myConnection);
        SqlDataReader nwReader = selectCommand.ExecuteReader();
        while (nwReader.Read())
        {
            string name = (string)nwReader["name"];
            //Response.Write(name);
            // Do something with UserID here...
        }
        nwReader.Close();
        myConnection.Close();


        List<Names> names = new List<Names>();
        DataSet ds = new DataSet();
        myConnection.Open();
        query = "SELECT TOP (10) [EMPLID],[Name] FROM [ctcLink_ODS].[dbo].[Names_HC]";

        SqlCommand cmd = new SqlCommand(query, myConnection);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //names.Add(new Names(int.Parse(dr["StudentID"].ToString()), dr["StudentName"].ToString()));
                //Response.Write(dr["EMPLID"].ToString());
            }

        }
        nwReader.Close();
        myConnection.Close();
    }
    

    [WebMethod]
    public static string GetList()
    {
        List<Names> names = new List<Names>();
        DataSet ds = new DataSet();
        string strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalEAN;Initial Catalog=CCSEAN;Integrated Security=true;";
        SqlConnection myConnection = new SqlConnection(strConnection);
        myConnection.Open();
        string query = "SELECT TOP (10) [EMPLID],[Name] FROM [ctcLink_ODS].[dbo].[Names_HC]";

        SqlCommand cmd = new SqlCommand(query, myConnection);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        var result = "";
        if (ds != null && ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                names.Add(new Names(int.Parse(dr["EMPLID"].ToString()), dr["Name"].ToString()));
                result += dr["Name"].ToString();
            }  
        }
        //return names;
        //var jsondata = new JavaScriptSerializer().Serialize(names);
        return result;
        //return jsondata;
    }

    
    
    
}

public class Names
{
    public int StudentID;
    public string StudentName;
    public Names(int _StudentID, string _StudentName)
    {
        StudentID = _StudentID;
        StudentName = _StudentName;
    }
}